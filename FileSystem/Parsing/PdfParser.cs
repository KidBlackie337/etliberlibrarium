﻿using EtLiberLibrarium.FileSystem.Validators;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Text;
using System.IO;
using System.Drawing;
using Tesseract;
using Ghostscript.NET.Rasterizer;
using System.Drawing.Imaging;
using System.Threading;
using Ghostscript.NET;

namespace EtLiberLibrarium.FileSystem.Parsing
{
    public abstract class PdfParser
    {
        public int MinPages { get; set; } = 10;

        /// <summary>
        /// <para>Reads the file at <paramref name="fileName"/> for text.</para>
        /// Returns the text found in the first <paramref name="depth"/> pages.
        /// By default <paramref name="depth"/> >= MinPages.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        public abstract string GetText(string fileName);

        public abstract bool GetIsbn(string fileName, out string isbn);

        public bool IsValidPdf(string fileName)
        {

            //try
            //{
            //    using (var reader = new PdfReader(fileName))
            //        return true;
            //}
            //catch(Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //    return false;
            //}
            var fi = new FileInfo(fileName);
            return fi.Extension == ".pdf";
        }

        /// <summary>
        /// Returns whether or not a valid ISBN was located in the text.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="isbn"></param>
        /// <returns></returns>
        public bool FindIsbn(string text, out string isbn, bool convert10To13 = true)
        {
            isbn = String.Empty;
            //If the text is too short for an isbn there can't be one.
            if (text.Length < 10)
                return false;

            int start = 0;
            int end = 1;
            bool foundIsbn = false;
            //If the text isn't labeled with ISBN or e-ISBN there probably
            //isn't one.
            if (text.Contains("ISBN"))
            {
                start = text.IndexOf("ISBN") + 4;
                end = start + 30;
                foundIsbn = true;
            }
            else if (text.Contains("e-ISBN"))
            {
                start = text.IndexOf("e-ISBN") + 6;
                end = start + 30;
                foundIsbn = true;
            }
            else if(text.Contains("International Standard Book Number"))
            {
                start = text.IndexOf("International Standard Book Number") + 6;
                end = start + 30;
                foundIsbn = true;
            }
            //Get the slice of text where the isbn should be
            //read in the isbn
            if(foundIsbn)
                isbn = ReadIsbn(text[start..end]);
            //return whether the isbn is valid ISBN-10 or ISBN-13
            if (isbn.Length == 10)
            {
                if (ValidateAndConvertIsbn(isbn, out string newIsbn))
                {
                    isbn = newIsbn;
                    return true;
                }
            }
            return ValidateIsbn(isbn);
        }

        private string ReadIsbn(string text)
        {
            StringBuilder isbnString = new StringBuilder();
            text = text.Replace("-", "").Replace(" ", "");

            if (text[0..3].Contains(":"))
                text = text[(text.IndexOf(":") + 1)..];

            for (int i = 0; i < text.Length; i++)
            {
                //Is the character a digit
                if (text[i] > 47 && text[i] < 58)
                {
                    isbnString.Append(text[i]);
                }
                //Is it the last potential index
                //If so is the last character the representation of 10
                else if ((i == 9 || i == 12) && text[i] == 'X')
                {
                    isbnString.Append(text[i]);
                    return isbnString.ToString();
                }
                //Is it the last index for an ISBN
                else if (isbnString.Length == 9 || isbnString.Length == 12)
                {
                    //Look ahead one character
                    //If the next character is invalid it's an isbn
                    if (!char.IsDigit(text[i + 1]))
                        return isbnString.ToString();
                }
                else if (!char.IsDigit(text[i]))
                    return isbnString.ToString();
            }

            return isbnString.ToString();
        }

        private bool ValidateIsbn(string isbnString)
        {
            IsbnValidator val;
            if (isbnString.Length != 13 && isbnString.Length != 10)
                return false;
            int[] digits = ParseIsbn(isbnString);
            if (digits.Length == 13)
                val = new Isbn13Validator();
            else
                val = new Isbn10Validator();
            return val.ValidateIsbn(digits);
        }

        private bool ValidateAndConvertIsbn(string isbnString, out string newIsbn)
        {
            IsbnValidator val;
            int[] digits = new int[13];
            newIsbn = isbnString;
            int length = isbnString.Length;
            if (length != 13 && length != 10)
                return false;
            int[] rawDigits = ParseIsbn(isbnString);
            if (length == 10)
            {
                val = new Isbn10Validator();
                if (val.ValidateAndConvert(rawDigits, out newIsbn))
                    return true;
            }
            else
            {
                val = new Isbn13Validator();
                if (val.ValidateIsbn(rawDigits))
                {
                    digits = rawDigits;
                    return true;
                }
            }

            return false;
        }

        private int[] ParseIsbn(string isbnString)
        {
            int[] digits = new int[isbnString.Length];

            for (int i = 0; i < digits.Length; i++)
            {
                if (isbnString[i] == 'X')
                    digits[i] = 10;
                else
                    digits[i] = (int)char.GetNumericValue(isbnString[i]);
            }

            return digits;
        }
    }

    public class RawTextPdfParser : PdfParser
    {
        public override bool GetIsbn(string fileName, out string isbn)
        {
            var fi = new FileInfo(fileName);
            isbn = String.Empty;

            if (!IsValidPdf(fileName))
                return false;

            using (var reader = new PdfReader(fileName))
            {
                for (int i = 1; i < MinPages + 1; i++)
                {
                    string pageText = PdfTextExtractor.GetTextFromPage(reader, i, new LocationTextExtractionStrategy());
                    if (FindIsbn(pageText, out isbn))
                    {
                        return true;
                    }
                }
            }


            return false;
        }

        public override string GetText(string fileName)
        {
            StringBuilder docText = new StringBuilder();

            if (!IsValidPdf(fileName))
                return String.Empty;

            using (PdfReader reader = new PdfReader(fileName))
            {
                for (int i = 1; i < MinPages; i++)
                {
                    try
                    {
                        string pageText = PdfTextExtractor.GetTextFromPage(reader, i, new LocationTextExtractionStrategy());
                        docText.AppendLine(pageText);
                    }
                    catch (NotSupportedException e)
                    {
                        continue;
                    }
                }
            }

            return docText.ToString();
        }
    }

    public class OcrPdfParser : PdfParser
    {
        public static string GhostscriptLocation => System.IO.Path.Combine(Directory.GetCurrentDirectory(), "ghostscript", "gsdll64.dll");
        public static string TessDataPath => System.IO.Path.Combine(Directory.GetCurrentDirectory(), "tessdata");

        public override string GetText(string fileName)
        {
            if (!IsValidPdf(fileName))
                return String.Empty;

            StringBuilder sb = new StringBuilder();

            for(int i = 1; i < MinPages + 1; i++)
            {
                sb.AppendLine(ConvertImageToText(ConvertPageToTiff(fileName, i)));
            }

            return sb.ToString();
        }

        public override bool GetIsbn(string fileName, out string isbn)
        {
            var fi = new FileInfo(fileName);
            isbn = String.Empty;

            if (!IsValidPdf(fileName))
                return false;
            try
            {
                for (int i = 1; i < MinPages + 1; i++)
                {
                    string pageText = ConvertImageToText(ConvertPageToTiff(fileName, i));
                    if (FindIsbn(pageText, out isbn))
                    {
                        return true;
                    }
                }
            }
            catch(GhostscriptException e)
            {
                return false;
            }

            return false;
        }

        private byte[] ConvertPageToTiff(string fileName, int pageNum)
        {
            using(var rasterizer = new GhostscriptRasterizer())
            {
                rasterizer.Open(fileName, new Ghostscript.NET.GhostscriptVersionInfo(GhostscriptLocation), true);
                using(var bitmap = new Bitmap(rasterizer.GetPage(150, pageNum)))
                {
                    using(var ms = new MemoryStream())
                    {
                        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Tiff);
                        return ms.ToArray();
                    }
                }
            }
        }

        private string ConvertImageToText(byte[] imageBytes)
        {
            string text;
            using(var ocr = new TesseractEngine(TessDataPath, "eng"))
            {
                using(var pixImg = Pix.LoadTiffFromMemory(imageBytes))
                {
                    var result = ocr.Process(pixImg);
                    text = result.GetText();
                    return text;
                }
            }
        }
    }
}