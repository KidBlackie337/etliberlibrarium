﻿using EtLiberLibrarium.Data;
using EtLiberLibrarium.Data.Models;
using EtLiberLibrarium.FileSystem.Parsing;
using EtLiberLibrarium.Scrapers;
using iTextSharp.text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Org.BouncyCastle.Asn1.X509;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace EtLiberLibrarium.FileSystem
{
    /// <summary>
    /// Searches directories for all pdfs.
    /// </summary>
    public class DirectoryReader
    {
        private static int _filesAddedToDb = 0;
        private static int _fileCount = 0;

        private static ConcurrentQueue<string> _dirs = new ConcurrentQueue<string>();
        private static ConcurrentQueue<string> _files = new ConcurrentQueue<string>();

        private static ConcurrentBag<string> _hashes = new ConcurrentBag<string>();
        private static ConcurrentBag<string> _isbns = new ConcurrentBag<string>();

        private DbContextOptions<BooksContext> _contextOptions;

        private ILogger _logger;

        public DirectoryReader(string rootDir, DbContextOptions<BooksContext> options, ILogger<DirectoryReader> logger = null)
        {
            _logger = logger;
            _dirs.Enqueue(rootDir);
            _contextOptions = options;
            using (var context = new BooksContext(_contextOptions))
            {
                if (context.Books.Count() != 0)
                {
                    foreach (var book in context.Books)
                    {
                        _hashes.Add(book.Hash);
                        _isbns.Add(book.ISBN);
                    }
                }
            }
            ThreadPool.QueueUserWorkItem(StartProcessing);
        }

        #region Manager
        private void StartProcessing(Object stateInfo)
        {
            int threadCount = 10;
            if (_dirs.TryDequeue(out string rootDir))
            {
                foreach (var dir in Directory.GetDirectories(rootDir))
                    _dirs.Enqueue(dir);
                for (int i = 0; i < threadCount; i++)
                    ThreadPool.QueueUserWorkItem(ProcessDirectory);
            }

            while (_dirs.Count != 0)
                Thread.Sleep(200);
            int _fileCount = _files.Count;

            for (int i = 0; i < threadCount; i++)
            {
                Thread thread = new Thread(ProcessFiles);
                thread.Name = i.ToString();
                thread.Start();
            }

            while (_files.Count != 0)
                Thread.Sleep(200);
        }

        private void ProcessDirectory(Object stateInfo)
        {
            if (_dirs.TryDequeue(out string dir))
            {
                foreach (var directory in Directory.GetDirectories(dir))
                {
                    _dirs.Enqueue(directory);
                }
                foreach (var file in Directory.GetFiles(dir))
                {
                    var fi = new FileInfo(file);
                    if(fi.Extension == ".pdf")
                        _files.Enqueue(file);
                }
            }
        }

        private void ProcessFiles(Object stateInfo)
        {
            while (_files.Count != 0)
            {
                if (_files.TryDequeue(out string file))
                {
                    try
                    {
                        if (TryAddingPdf(file))
                        {
                            _filesAddedToDb++;

                            var fi = new FileInfo(file);
                            Console.WriteLine($"Thread Id: {Thread.CurrentThread.Name}\nAdded file to db." +
                                $"\n{_filesAddedToDb} out of {_fileCount} added");
                            if(_filesAddedToDb == _fileCount)
                            {
                                _fileCount = 0;
                                _filesAddedToDb = 0;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        #endregion
        #region FileParsing
        private bool TryAddingPdf(string filePath, int depth = 10)
        {
            string hash = ConvertChecksum(GenerateChecksum(filePath));
            string isbn = String.Empty;


            if (_hashes.Contains(hash))
                return false;
            PdfParser parser = new RawTextPdfParser();
            if (!TryParsingIsbn(filePath, parser, ref isbn))
            {
                parser = new OcrPdfParser();
                if (!TryParsingIsbn(filePath, parser, ref isbn))
                    return false;
            }
            Book book = new Book { ISBN = isbn, Hash = hash, Path = filePath };
            return TryAdding(book);
        }
        private bool TryParsingIsbn(string filePath, PdfParser parser, ref string isbn, int depth = 10)
        {
            if(parser.GetIsbn(filePath, out string foundIsbn))
            {
                isbn = foundIsbn;
                return true;
            }
            return false;
        }

        private byte[] GenerateChecksum(string pathToFile)
        {
            if (!File.Exists(pathToFile))
                throw new FileNotFoundException();

            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(pathToFile))
                {
                    return md5.ComputeHash(stream);
                }
            }
        }

        private string ConvertChecksum(byte[] md5Sum)
        {
            return BitConverter.ToString(md5Sum).Replace("-", "");
        }

        #endregion
        #region Database
        public bool TryAdding<T>(T entity) where T: DbEntity
        {
            try
            {
                using(var context = new BooksContext(_contextOptions))
                {
                    switch (entity.Type)
                    {
                        case EntityType.Book:
                            Book e = entity as Book;
                            AddBook(e);
                            break;
                        case EntityType.Author:
                            OpenLibAuthorInfo ai = entity as OpenLibAuthorInfo;
                            context.Authors.Add(ai);
                            break;
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private void AddBook(Book book)
        {
            using (var context = new BooksContext(_contextOptions))
            {
                var bookMeta = OpenLibrary.GetBookMetadataAsObject(book.ISBN).Result;
                book.Info = bookMeta;
                book.AddMetadata();
                StringBuilder authorSb = new StringBuilder();
                foreach(var author in bookMeta.Authors)
                {
                    context.Authors.Add(author);
                    context.SaveChanges();
                    authorSb.Append($"{author.AuthorId};;");
                }
                book.AuthorString = authorSb.ToString();
                context.Books.Add(book);
                context.SaveChanges();
            }
        }

        #endregion
    }
}