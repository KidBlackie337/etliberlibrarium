﻿using EtLiberLibrarium.Data;
using EtLiberLibrarium.Data.Models;
using EtLiberLibrarium.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EtLiberLibrarium.Controllers
{
    public class DatabaseController : Controller
    {
        private readonly ILogger<DatabaseController> _logger;
        private BooksContext _db;

        public DatabaseController(ILogger<DatabaseController> logger, BooksContext injectedContext)
        {
            _logger = logger;
            _db = injectedContext;
        }

        public async Task<IActionResult> Index()
        {
            var model = new DatabaseIndexViewModel()
            {
                Books = await _db.Books.ToListAsync()
            };
            return View(model);
        }

        public IActionResult Search()
        {
            var model = new DatabaseIndexViewModel()
            {
                Books = _db.Books.ToList()
            };
            return View(model);
        }

        public IActionResult Upload()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> ViewBook([FromQuery]int itemid)
        {
            var book = await _db.Books.FindAsync(itemid);
            if (book == null)
                book = new Book();
            var model = new DatabaseBookViewModel()
            {
                Book = book
            };
            return View(model);
        }
    }
}
