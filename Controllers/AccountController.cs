﻿using EtLiberLibrarium.Data;
using EtLiberLibrarium.Data.Models;
using EtLiberLibrarium.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EtLiberLibrarium.Controllers
{
    public class AccountController : Controller
    {
        private BooksContext _db;

        public AccountController(BooksContext injectedContext)
        {
            _db = injectedContext;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
    }
}
