﻿using EtLiberLibrarium.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EtLiberLibrarium.Models
{
    public class DatabaseIndexViewModel
    {
        public IList<Book> Books { get; set; }
    }
}
