﻿using EtLiberLibrarium.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EtLiberLibrarium.Models
{
    public class DatabaseBookViewModel
    {
        public Book Book { get; set; }
    }
}
