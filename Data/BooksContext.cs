﻿using EtLiberLibrarium.Data.Models;
using EtLiberLibrarium.Scrapers;
using iTextSharp.text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace EtLiberLibrarium.Data
{
    public class BooksContext : IdentityDbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<OpenLibAuthorInfo> Authors { get; set; }

        

        public BooksContext(DbContextOptions<BooksContext> options) : base(options)
        {
            Console.WriteLine($"{ContextId} Has been created.");
        }
    }
}