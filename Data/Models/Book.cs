﻿using EtLiberLibrarium.Scrapers;
using iTextSharp.text;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EtLiberLibrarium.Data.Models
{
    public enum EntityType
    {
        Book,
        Author,
        Account
    }

    public abstract class DbEntity
    {
        [NotMapped]
        public abstract EntityType Type { get; }
    }

    public class Book : DbEntity
    {
        [NotMapped]
        public override EntityType Type { get => EntityType.Book; } 
        [Key]
        public int BookId { get; set; }
        public string Hash { get; set; }
        public string ISBN { get; set; }
        public string Title { get; set; }
        public string SubjectsString { get; set; }
        public string PublisherString { get; set; }
        public string SeriesString { get; set; }
        public string AuthorString { get; set; }
        public string Languages { get; set; }
        public int Pages { get; set; }
        public string Path { get; set; }
        [NotMapped]
        public OpenLibBookInfo Info { get; set; }
        [NotMapped]
        public List<OpenLibAuthorInfo> Authors { get; set; }

        public void AddMetadata()
        {
            Title = Info.Title;
            Languages = Info.LanguageString;
            Pages = Info.Pages;
            PublisherString = Info.PublishersString;
            SeriesString = Info.SeriesString;
            SubjectsString = Info.SubjectsString;
        }

        public List<string> GetAuthorNames(BooksContext context)
        {
            List<string> authorNames = new List<string>();
            var authorKeys = AuthorString.Split(";;");
            return authorNames;
        }
    }
}