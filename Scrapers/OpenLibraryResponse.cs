﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc.Routing;
using System.Text;
using EtLiberLibrarium.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace EtLiberLibrarium.Scrapers
{
    public class OpenLibraryResponse
    {
        [JsonProperty("bib_key")]
        public string BibKey { get; set; }

        [JsonProperty("info_url")]
        public string InfoUrl { get; set; }

        [JsonProperty("preview")]
        public string Preview { get; set; }

        [JsonProperty("preview_url")]
        public string PreviewUrl { get; set; }

        [JsonProperty("thumbnail_url")]
        public string ThumbnailUrl { get; set; }
    }

    public class OpenLibAuthor
    {
        [JsonProperty("key")]
        public string Key { get; set; }
    }

    public class OpenLibLanguage
    {
        [JsonProperty("key")]
        public string Key { get; set; }
    }

    public class OpenLibBookInfo
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonIgnore]
        private List<OpenLibAuthor> _authorKeys = new List<OpenLibAuthor>();
        [JsonProperty("authors")]
        public List<OpenLibAuthor> AuthorKeys { get => _authorKeys; set{
                _authorKeys = value;
            } 
        }
        public List<OpenLibAuthorInfo> Authors { get; set; }
        [JsonProperty("publishers")]
        public List<string> Publishers { get; set; } = new List<string>();
        [JsonIgnore]
        public string PublishersString => MergeList(Publishers);
        [JsonProperty("number_of_pages")]
        public int Pages { get; set; }
        [JsonProperty("subjects")]
        public List<string> Subjects { get; set; } = new List<string>();
        [JsonIgnore]
        public string SubjectsString => MergeList(Subjects);
        [JsonProperty("series")]
        public List<string> Series { get; set; } = new List<string>();
        [JsonIgnore]
        public string SeriesString => MergeList(Series);
        [JsonProperty("languages")]
        public List<OpenLibLanguage> Languages { get; set; } = new List<OpenLibLanguage>();
        [JsonIgnore]
        public List<string> LanguageKeys { get; set; }
        public string LanguageString => MergeList(LanguageKeys);

        public OpenLibBookInfo()
        {
            SetAuthors();
            SetLanguages();
        }

        private void SetLanguages()
        {
            LanguageKeys = new List<string>();
            foreach (var l in Languages)
                LanguageKeys.Add(l.Key);
        }
        private void SetAuthors()
        {
            Authors = new List<OpenLibAuthorInfo>();
            List<string> authorKeys = new List<string>();
            foreach (var author in _authorKeys)
                authorKeys.Add(author.Key);
            Authors.AddRange(OpenLibrary.GetAuthorsAsObjects(authorKeys).Result);
        }

        private string MergeList(List<string> stringList)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in stringList)
                sb.Append(s + ";;");
            return sb.ToString();
        }
    }

    public class OpenLibAuthorInfo : DbEntity
    {
        [NotMapped]
        public override EntityType Type { get => EntityType.Author; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [Key]
        [JsonProperty("id")]
        public int AuthorId { get; set; }

        [NotMapped]
        [JsonProperty("key")]
        public string Key { get; set; }

    }
}
