﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace EtLiberLibrarium.Scrapers
{
    public static class OpenLibrary
    {
        private static HttpClient _client;
        public static HttpClient Client {
            get {
                if (_client == null)
                    _client = new HttpClient();
                return _client;
            }
        }
        public static string BaseRequestUrl => "https://openlibrary.org";

        public static async Task<OpenLibraryResponse> GetResponseWithIsbn(string isbn)
        {
            string request = BaseRequestUrl + $"/api/books?bibkeys=ISBN:{isbn}&&callback=mycallback";
            HttpResponseMessage response = await Client.GetAsync(request);
            string responseString = await response.Content.ReadAsStringAsync();
            responseString = responseString[(responseString.IndexOf('{') + 23)..^3];
            var openLib = JsonConvert.DeserializeObject<OpenLibraryResponse>(responseString);
            return openLib;
        }

        public static async Task<string> GetAuthorAsString(string authorKey)
        {
            string request = BaseRequestUrl + authorKey + ".json";
            HttpResponseMessage response = await Client.GetAsync(request);
            string responseString = await response.Content.ReadAsStringAsync();
            return responseString;
        }

        public static async Task<List<OpenLibAuthorInfo>> GetAuthorsAsObjects(List<string> authorsKey)
        {
            List<OpenLibAuthorInfo> authors = new List<OpenLibAuthorInfo>();
            foreach(var key in authorsKey)
            {
                authors.Add(JsonConvert.DeserializeObject<OpenLibAuthorInfo>(await GetAuthorAsString(key)));
            }
            return authors;
        }

        public static async Task<string> GetBookMetadataAsString(string isbn)
        {
            string request = BaseRequestUrl + $"/isbn/{isbn}.json";
            HttpResponseMessage rMessage = await Client.GetAsync(request);
            string rString = await rMessage.Content.ReadAsStringAsync();
            return rString;
        }

        public static async Task<OpenLibBookInfo> GetBookMetadataAsObject(string isbn)
        {
            string json = await GetBookMetadataAsString(isbn);
            var bookInfo = JsonConvert.DeserializeObject<OpenLibBookInfo>(json);
            return bookInfo;
        }

        public static async Task<string> GetBookUrl(string isbn)
        {
            var obj = await GetResponseWithIsbn(isbn);
            return obj.InfoUrl;
        }
    }
}
